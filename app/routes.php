<?php

use Symfony\Component\HttpFoundation\Request;
use MicroCMS\Domain\Comment;
use MicroCMS\Domain\Chapitre;
use MicroCMS\Domain\Admin;
use MicroCMS\Form\CommentType;
use MicroCMS\Form\ChapitreType;
use MicroCMS\Form\AdminType;
use MicroCMS\Domain\Message;
use MicroCMS\Form\MessageType;

// Home page
$app->get('/', function () use ($app) {
	$chapitres = $app['dao.chapitre']->findLasts();
	return $app['twig']->render('index.html.twig', array('chapitres' => $chapitres));
})->bind('home');

// About page
$app->get('/a-propos', function () use ($app) {
	return $app['twig']->render('about.html.twig');
})->bind('about');

$app->get('/chapitres/{id}', function ($id) use ($app) {
	$count = $app['dao.chapitre']->count();
	$total = $count['total'];
	$perPage = 4;
	$currentPage = $id;
	$nbPage = ceil($total/$perPage);
	$limit = ($currentPage - 1)*$perPage;
	$pages = [];

	for ($i=1; $i<= $nbPage; $i++) {
		$pages[] = $i;
	}

	$chapitres = $app['dao.chapitre']->findBy($limit, $perPage);

	return $app['twig']->render(
		'chapitres.html.twig',
		array(
			'chapitres' => $chapitres,
			'pages' => $pages,
			'current' => $currentPage
		)
	);
})->bind('chapitres');

// Article details with comments
$app->match('/chapitre/{slug}', function ($slug, Request $request) use ($app) {
	$chapitre = $app['dao.chapitre']->findBySlug($slug);
	$comment = new Comment();
	$comment->setChapitre($chapitre);
	$commentForm = $app['form.factory']->create(CommentType::class, $comment);
	$commentForm->handleRequest($request);
	if ($commentForm->isSubmitted() && $commentForm->isValid()) {
		$data = $commentForm->getData();
		$comment->setPseudo($data->getPseudo());
		$comment->setContent($data->getContent());
		$comment->setLikes(0);
		$comment->setSignals(0);
		$app['dao.comment']->save($comment);
		$app['session']->getFlashBag()->add('success', "Votre commentaire a été ajouté avec succès.");
	}
	$comments = $app['dao.comment']->findAllByChapitre($chapitre->getId());
	return $app['twig']->render(
		'chapitre.html.twig',
		array(
			'chapitre' => $chapitre,
			'comments' => $comments,
			'commentForm' => $commentForm->createView()
		)
	);
})->bind('chapitre');

$app->match('chapitre/{chapitre}/signaler/{id}', function ($chapitre, $id) use ($app) {
	$comment = $app['dao.comment']->signal($id);
	$app['dao.comment']->save($comment);
	return $app->redirect($app['url_generator']->generate('chapitre', array('slug' => $chapitre)));
})->bind('signaler');

$app->match('chapitre/{chapitre}/liker/{id}', function ($chapitre, $id) use ($app) {
	$comment = $app['dao.comment']->like($id);
	$app['dao.comment']->save($comment);
	return $app->redirect($app['url_generator']->generate('chapitre', array('slug' => $chapitre)));

})->bind('liker');

// Contact
$app->match('/contact', function (Request $request) use ($app) {
	$message = new Message();
	$form = $app['form.factory']->create(MessageType::class, $message);
	$form->handleRequest($request);
	if ($form->isSubmitted() && $form->isValid()) {
		$data = $form->getData();
		$dateCreation = new DateTime();
		$dateMesage = $dateCreation->format('Y-m-d H:i:s');
		$message->setDateCreation($dateMesage);
		$app['dao.message']->save($message);
		$app['session']->getFlashBag()->add('success', "Votre message a été envoyé avec succès !");
	}
	return $app['twig']->render(
		'contact.html.twig',
		array(
			'form' => $form->createView()
		)
	);
})->bind('contact');

// Login form
$app->get('/login', function(Request $request) use ($app) {
	return $app['twig']->render('login.html.twig', array(
		'error'         => $app['security.last_error']($request),
		'last_username' => $app['session']->get('_security.last_username'),
	));
})->bind('login');


// Admin home page
$app->get('/admin', function() use ($app) {
	$chapitres = $app['dao.chapitre']->findAll();
	$comments = $app['dao.comment']->findAll();
	$users = $app['dao.admin']->findAll();
	$messages = $app['dao.message']->findAll();
	return $app['twig']->render('admin.html.twig', array(
		'chapitres' => $chapitres,
		'comments' => $comments,
		'users' => $users,
		'messages' => $messages
	));
})->bind('admin');

// Add a new chapter
$app->match('/admin/chapitre/add', function(Request $request) use ($app) {
	$chapitre = new Chapitre();
	$chapitreForm = $app['form.factory']->create(ChapitreType::class, $chapitre);
	$chapitreForm->handleRequest($request);
	if ($chapitreForm->isSubmitted() && $chapitreForm->isValid()) {
		$data = $chapitreForm->getData();
		$chapitre->setPublished($data->getPublished());
		$app['dao.chapitre']->save($chapitre);
		$app['session']->getFlashBag()->add('success', "Le chapitre a été créé avec succès.");
		return $app->redirect($app['url_generator']->generate('admin'));
	}
	return $app['twig']->render('chapitre_form.html.twig', array(
		'title' => 'Nouveau chapitre',
		'chapitreForm' => $chapitreForm->createView()));
})->bind('admin_chapitre_add');

// Edit an existing chapter
$app->match('/admin/chapitre/{id}/edit', function($id, Request $request) use ($app) {
	$chapitre = $app['dao.chapitre']->find($id);
	$chapitreForm = $app['form.factory']->create(ChapitreType::class, $chapitre);
	$chapitreForm->handleRequest($request);
	if ($chapitreForm->isSubmitted() && $chapitreForm->isValid()) {
		$data = $chapitreForm->getData();
		$chapitre->setPublished($data->getPublished());
		$app['dao.chapitre']->save($chapitre);
		$app['session']->getFlashBag()->add('success', "Le chapitre a été mis à jour.");
		return $app->redirect($app['url_generator']->generate('admin'));
	}
	return $app['twig']->render('chapitre_form.html.twig', array(
		'title' => 'Edit chapitre',
		'chapitreForm' => $chapitreForm->createView()));
})->bind('admin_chapitre_edit');

// Remove a chapter
$app->get('/admin/chapitre/{id}/delete', function($id, Request $request) use ($app) {
	// delete associated articles and comments ?
	// Delete the chapitre
	$app['dao.chapitre']->delete($id);
	$app['session']->getFlashBag()->add('success', "Le chapitre a été supprimé avec succès.");
	// Redirect to admin home page
	return $app->redirect($app['url_generator']->generate('admin'));
})->bind('admin_chapitre_delete');

// Make offline a chapter
$app->get('/admin/chapitre/{id}/offline', function ($id) use ($app) {
	$chapitre = $app['dao.chapitre']->find($id);
	$chapitre->setPublished(0);
	$app['dao.chapitre']->save($chapitre);
	$app['session']->getFlashBag()->add('success', "Le chapitre a été mis hors ligne.");
	return $app->redirect($app['url_generator']->generate('admin'));
})->bind('admin_chapitre_offline');

// Edit an existing comment
$app->match('/admin/comment/{id}/edit', function($id, Request $request) use ($app) {
	$comment = $app['dao.comment']->find($id);
	$likes = $comment->getLikes();
	$signals = $comment->getSignals();
	$pseudo = $comment->getPseudo();
	$commentForm = $app['form.factory']->create(CommentType::class, $comment);
	$commentForm->handleRequest($request);
	if ($commentForm->isSubmitted() && $commentForm->isValid()) {
		$data = $commentForm->getData();
		$app['dao.comment']->save($comment);
		$app['session']->getFlashBag()->add('success', "Le commentaire a été modifié avec succès.");
		return $app->redirect($app['url_generator']->generate('admin'));
	}
	return $app['twig']->render(
		'comment_form.html.twig',
		array(
			'title' => 'Modifier le commentaire',
			'comment' => $comment,
			'commentForm' => $commentForm->createView()
		)
	);
})->bind('admin_comment_edit');

// Remove a comment
$app->get('/admin/comment/{id}/delete', function($id) use ($app) {
	$app['dao.comment']->delete($id);
	$app['session']->getFlashBag()->add('success', "Le commentaire a été supprimé avec succès.");
	// Redirect to admin home page
	return $app->redirect($app['url_generator']->generate('admin'));
})->bind('admin_comment_delete');

// Remove a comment
$app->get('/admin/comment/{id}/unsignal', function($id) use ($app) {
	$app['dao.comment']->unsignal($id);
	$app['session']->getFlashBag()->add('success', "Vous avez signalé ce commentaire comme étant innofensif.");
	// Redirect to admin home page
	return $app->redirect($app['url_generator']->generate('admin'));
})->bind('admin_comment_unsignal');

// Add a user
$app->match('/admin/user/add', function(Request $request) use ($app) {
	$user = new Admin();
	$userForm = $app['form.factory']->create(AdminType::class, $user);
	$userForm->handleRequest($request);
	if ($userForm->isSubmitted() && $userForm->isValid()) {
		$data = $userForm->getData();
		// generate a random salt value
		$salt = substr(md5(time()), 0, 23);
		$user->setSalt($salt);
		$plainPassword = $user->getPassword();
		// find the default encoder
		$encoder = $app['security.encoder.bcrypt'];
		// compute the encoded password
		$password = $encoder->encodePassword($plainPassword, $user->getSalt());
		$user->setPassword($password);
		$user->setSlug($user->getUsername());
		$app['dao.admin']->save($user);
		$app['session']->getFlashBag()->add('success', "Utilisateur créé avec succès.");
		return $app->redirect($app['url_generator']->generate('admin'));
	}
	return $app['twig']->render('user_form.html.twig', array(
		'title' => 'New user',
		'userForm' => $userForm->createView()));
})->bind('admin_user_add');

// Edit an existing user
$app->match('/admin/user/{id}/edit', function($id, Request $request) use ($app) {
	$user = $app['dao.admin']->find($id);
	$userForm = $app['form.factory']->create(AdminType::class, $user);
	$userForm->handleRequest($request);
	if ($userForm->isSubmitted() && $userForm->isValid()) {
		$data = $userForm->getData();
		$plainPassword = $user->getPassword();
		// find the encoder for the user
		$encoder = $app['security.encoder_factory']->getEncoder($user);
		// compute the encoded password
		$password = $encoder->encodePassword($plainPassword, $user->getSalt());
		$user->setPassword($password);
		$user->setSlug($user->getUsername());
		$app['dao.admin']->save($user);
		$app['session']->getFlashBag()->add('success', "Utilisateur mis à jour avec succès.");
	}
	return $app['twig']->render('user_form.html.twig', array(
		'title' => 'Edit user',
		'userForm' => $userForm->createView()));
})->bind('admin_user_edit');

// Remove a user
$app->get('/admin/user/{id}/delete', function($id, Request $request) use ($app) {
	// Delete the user
	$app['dao.admin']->delete($id);
	$app['session']->getFlashBag()->add('success', "Utilisateur supprimé avec succès.");
	// Redirect to admin home page
	return $app->redirect($app['url_generator']->generate('admin'));
})->bind('admin_user_delete');

// View a message
$app->get('/admin/message/{id}', function($id) use ($app) {
	$message = $app['dao.message']->find($id);
	return $app['twig']->render('admin_message.html.twig', array(
		'message' => $message
	));
})->bind('admin_message');

$app->get('/admin/message/{id}/delete', function($id) use ($app) {
	$app['dao.message']->delete($id);
	$app['session']->getFlashBag()->add('success',"Message correctement supprimé");
	return $app->redirect($app['url_generator']->generate('admin'));
})->bind('admin_message_delete');