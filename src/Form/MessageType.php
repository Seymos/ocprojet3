<?php
namespace MicroCMS\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints;

class MessageType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add(
				'name',
				TextType::class,
				array(
					'label' => "Votre nom complet",
					'attr' => array(
						'class' => "form-control"
					),
					'constraints' => array(
						new Constraints\NotBlank(
							array('message' => "Ce champ est obligatoire !")
						),
						new Constraints\Regex(
							array(
								'pattern' => "/^[\w-]{3,}[\s]+[\w]{3,}$/",
								'match' => true,
								'message' => "Votre nom n'est pas valide."
							)
						)
					)
				)
			)
			->add(
				'email',
				EmailType::class,
				array(
					'label' => "Votre e-mail",
					'attr' => array(
						'class' => "form-control"
					),
					'constraints' => array(
						new Constraints\NotBlank(
							array('message' => "Ce champ est obligatoire !")
						),
						new Constraints\Regex(
							array(
								'pattern' => "/^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/",
								'match' => true,
								'message' => "Votre adresse e-mail est invalide."
							)
						)
					)
				)
			)
			->add(
				'message',
				TextareaType::class,
				array(
					'label' => "Votre message",
					'attr' => array(
						'class' => "form-control",
						'rows' => "10"
					),
					'constraints' => array(
						new Constraints\NotBlank(
							array('message' => "Ce champ est obligatoire !")
						),
						new Constraints\Length(
							array(
								'min' => 10,
								'max' => 500,
								'minMessage' => "Votre message est trop court.",
								'maxMessage' => "Votre message est trop long..."
							)
						)
					)
				)
			)
			->add(
				'dateCreation',
				HiddenType::class,
				array(
					'required' => false
				)
			)
			->add(
				'submit',
				SubmitType::class,
				array(
					'label' => "Envoyer",
					'attr' => array(
						'class' => "button"
					)
				)
			);
	}

	public function getName()
	{
		return 'message';
	}
}