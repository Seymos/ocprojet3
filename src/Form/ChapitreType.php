<?php

namespace MicroCMS\Form;

use Doctrine\DBAL\Types\BooleanType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints;

class ChapitreType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add(
				'title',
				TextType::class,
				array(
					'label' => "Titre du chapitre",
					'attr' => array(
						'class' => "form-control"
					),
					'constraints' => array(
						new Constraints\NotBlank(
							array(
								'message' => "Ce champ est obligatoire."
							)
						),
						new Constraints\Regex(
							array(
								'pattern' => "/^[\w-\s]{5,}$/",
								'match' => true,
								'message' => "Veuillez vérifier le titre svp."
							)
						)
					)
				)
			)
			->add(
				'summary',
				TextareaType::class,
				array(
					'label' => "Résumé du chapitre (facultatif)",
					'required' => false,
					'constraints' => array(
						new Constraints\Length(
							array(
								'min' => 50,
								'max' => 300,
								'minMessage' => "Votre résumé est trop court et doit dépasser les 50 caractères.",
								'maxMessage' => "Votre résumé ne doit pas dépasser 300 caractères."
							)
						)
					)
				)
			)
			->add(
				'content',
				TextareaType::class,
				array(
					'label' => "Votre chapitre (votre contenu)",
					'required' => false
				)
			)
			->add(
				'published',
				CheckboxType::class,
				array(
					'label' => "Publier ce chapitre ?",
					'required' => false
				)
			)
		;

		$builder
			->get('published')
			->addModelTransformer(new CallbackTransformer(
				function ($publishedAsBoolean) {
					if ($publishedAsBoolean === 1) {
						return true;
					} else {
						return false;
					}
				},
				function ($publishedAsTinyInt) {
					if ($publishedAsTinyInt === true) {
						return 1;
					} else {
						return 0;
					}
				}
			))
		;
	}

	public function getName()
	{
		return 'chapitre';
	}
}