<?php
namespace MicroCMS\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints;

class CommentType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add(
				'pseudo',
				TextType::class,
				array(
					'label' => "Votre nom",
					'attr' => array(
						'class' => "form-control"
					),
					'constraints' => array(
						new Constraints\NotBlank(
							array(
								'message' => "Ce champ est obligatoire."
							)
						),
						new Constraints\Regex(
							array(
								'pattern' => "/[a-zA-Z]{3,}/",
								'match' => true,
								'message' => "Votre nom doit contenir au moins 3 lettres."
							)
						)
					)
				)
			)
			->add(
				'content',
				TextareaType::class,
				array(
					'label' => "Votre commentaire",
					'attr' => array(
						'class' => "form-control"
					),
					'constraints' => array(
						new Constraints\NotBlank(
							array(
								'message' => "Ce champ est obligatoire."
							)
						),
						new Constraints\Length(
							array(
								'min' => 10,
								'max' => 300,
								'minMessage' => "Votre message doit contenir au moins 10 caractères.",
								'maxMessage' => "Votre message doit contenir au plus 300 caractères."
							)
						)
					)
				)
			)
			->add(
				'signals',
				HiddenType::class,
				array(
					'required' => false,
					'attr' => array(
						'value' => 0
					)
				)
			)
			->add(
				'likes',
				HiddenType::class,
				array(
					'required' => false,
					'attr' => array(
						'value' => 0
					)
				)
			)
			->add(
				'submit',
				SubmitType::class,
				array(
					'label' => "Valider le commentaire",
					'attr' => array(
						'class' => 'button'
					)
				)
			)
		;
	}

	public function getName()
	{
		return 'comment';
	}
}
