<?php

namespace MicroCMS\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints;

class AdminType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add(
				'username',
				TextType::class,
				array(
					'label' => "Nom d'utilisateur",
					'constraints' => array(
						new Constraints\NotBlank(
							array(
								'message' => "Ce champ est obligatoire."
							)
						),
						new Constraints\Length(
							array(
								'min' => 3,
								'max' => 15,
								'minMessage' => "Votre nom d'utilisateur est trop court.",
								'maxMessage' => "Votre nom d'utilisateur est trop long."
							)
						)
					)
				)
			)
			->add(
				'slug',
				HiddenType::class,
				array(
					'required' => false
				)
			)
			->add(
				'password',
				RepeatedType::class,
				array(
					'type'            => PasswordType::class,
					'invalid_message' => 'The password fields must match.',
					'options'         => array('required' => true),
					'first_options'   => array('label' => 'Mot de passe'),
					'second_options'  => array('label' => 'Répéter le mot de passe'),
				)
			)
			->add(
				'role',
				ChoiceType::class,
				array(
					'choices' => array(
						'Admin' => 'ROLE_ADMIN',
						'User' => 'ROLE_USER'
					)
				)
			)
			->add(
				'submit',
				SubmitType::class,
				array(
					'label' => "Valider"
				)
			)
		;
	}

	public function getName()
	{
		return 'user';
	}
}
