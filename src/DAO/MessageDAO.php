<?php
namespace MicroCMS\DAO;

use MicroCMS\Domain\Message;

class MessageDAO extends DAO {

	/**
	 * Returns a list of all messages, sorted by date (most recent first).
	 *
	 * @return array A list of all messages.
	 */
	public function findAll() {
		$sql = "select * from message order by id desc";
		$result = $this->getDb()->fetchAll($sql);

		// Convert query result to an array of domain objects
		$entities = array();
		foreach ($result as $row) {
			$id = $row['id'];
			$entities[$id] = $this->buildDomainObject($row);
		}
		return $entities;
	}



	/**
	 * Returns a message matching the supplied id.
	 *
	 * @param integer $id The message id
	 *
	 * @return \MicroCMS\Domain\Message|throws an exception if no matching message is found
	 */
	public function find($id) {
		$sql = "select * from message where id=?";
		$row = $this->getDb()->fetchAssoc($sql, array($id));

		if ($row)
			return $this->buildDomainObject($row);
		else
			throw new \Exception("No message matching id " . $id);
	}

	/**
	 * Removes a message from the database.
	 *
	 * @param @param integer $id The message id
	 */
	public function delete($id) {
		// Delete the message
		$this->getDb()->delete('message', array('id' => $id));
	}

	/**
	 * Saves a message into the database.
	 *
	 * @param \MicroCMS\Domain\Message $message The message to save
	 */
	public function save(Message $message) {
		$messageData = array(
			'name' => $message->getName(),
			'email' => $message->getEmail(),
			'message' => $message->getMessage(),
			'dateCreation' => $message->getDateCreation()
		);

		if ($message->getId()) {
			// The message has already been saved : update it
			$this->getDb()->update('message', $messageData, array('id' => $message->getId()));
		} else {
			// The comment has never been saved : insert it
			$this->getDb()->insert('message', $messageData);
			// Get the id of the newly created comment and set it on the entity.
			$id = $this->getDb()->lastInsertId();
			$message->setId($id);
		}
	}

	/**
	 * Creates an Message object based on a DB row.
	 *
	 * @param array $row The DB row containing Message data.
	 * @return \MicroCMS\Domain\Message
	 */
	protected function buildDomainObject(array $row) {
		$message = new Message();
		$message->setId($row['id']);
		$message->setName($row['name']);
		$message->setEmail($row['email']);
		$message->setMessage($row['message']);
		$message->setDateCreation($row['dateCreation']);

		return $message;
	}
}