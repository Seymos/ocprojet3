<?php
namespace MicroCMS\DAO;

use MicroCMS\Domain\Chapitre;
use Cocur\Slugify\Slugify;

class ChapitreDAO extends DAO
{
	/**
	 * Return a list of all chapters, sorted by date (most recent first).
	 *
	 * @return array A list of all chapters.
	 */
	public function findAll() {
		$sql = "select * from chapitre order by id desc";
		$result = $this->getDb()->fetchAll($sql);

		// Convert query result to an array of domain objects
		$chapitres = $this->convert($result);
		return $chapitres;
	}

	/**
	 * Return a list of all chapters, sorted by date (most recent first).
	 *
	 * @return array A list of all chapters.
	 */
	public function findBy($limit, $perpage) {
		$sql = "select * from chapitre where published = 1 order by id desc limit $limit, $perpage";
		$result = $this->getDb()->fetchAll($sql);
		// Convert query result to an array of domain objects
		$chapitres = $this->convert($result);
		return $chapitres;
	}

	public function convert($result) {
		$chapitres = array();
		foreach ($result as $row) {
			$chapitreId = $row['id'];
			$chapitres[$chapitreId] = $this->buildDomainObject($row);
		}
		return $chapitres;
	}

	/**
	 * Return a list of all chapters, sorted by date (most recent first).
	 *
	 * @return array A list of all chapters.
	 */
	public function findLasts() {
		$sql = "select * from chapitre where published = 1 order by id desc limit 3";
		$result = $this->getDb()->fetchAll($sql);

		// Convert query result to an array of domain objects
		$chapitres = array();
		foreach ($result as $row) {
			$chapitreId = $row['id'];
			$chapitres[$chapitreId] = $this->buildDomainObject($row);
		}
		return $chapitres;
	}

	/**
	 * Returns an chapter matching the supplied id.
	 *
	 * @param integer $id
	 *
	 * @return \MicroCMS\Domain\Chapitre|throws an exception if no matching chapter is found
	 */
	public function findBySlug($slug) {
		$sql = "select * from chapitre where slug=?";
		$row = $this->getDb()->fetchAssoc($sql, array($slug));

		if ($row)
			return $this->buildDomainObject($row);
		else
			throw new \Exception("No chapter matching : " . $slug);
	}

	/**
	 * Returns an chapter matching the supplied id.
	 *
	 * @param integer $id
	 *
	 * @return \MicroCMS\Domain\Chapitre|throws an exception if no matching chapter is found
	 */
	public function find($id) {
		$sql = "select * from chapitre where id=?";
		$row = $this->getDb()->fetchAssoc($sql, array($id));

		if ($row)
			return $this->buildDomainObject($row);
		else
			throw new \Exception("No chapter matching : " . $id);
	}

	/**
	 * Return a list of all chapters, sorted by date (most recent first).
	 *
	 * @return array A list of all chapters.
	 */
	public function count() {
		$sql = "select COUNT(id) as total from chapitre";
		$result = $this->getDb()->fetchAssoc($sql);
		return $result;
	}

	/**
	 * Saves a chapter into the database.
	 *
	 * @param \MicroCMS\Domain\Chapitre $chapitre The article to save
	 */
	public function save(Chapitre $chapitre) {
		$slugify = new Slugify();
		$chapitreData = array(
			'title' => $chapitre->getTitle(),
			'slug' => $slugify->slugify($chapitre->getTitle()),
			'summary' => $chapitre->getSummary(),
			'content' => $chapitre->getContent(),
			'published' => $chapitre->getPublished()
		);

		if ($chapitre->getId()) {
			// The article has already been saved : update it
			$this->getDb()->update('chapitre', $chapitreData, array('id' => $chapitre->getId()));
		} else {
			// The article has never been saved : insert it
			$this->getDb()->insert('chapitre', $chapitreData);
			// Get the id of the newly created article and set it on the entity.
			$id = $this->getDb()->lastInsertId();
			$chapitre->setId($id);
		}
	}

	/**
	 * Removes an article from the database.
	 *
	 * @param integer $id The article id.
	 */
	public function delete($id) {
		// Delete the article
		$this->getDb()->delete('chapitre', array('id' => $id));
	}

	/**
	 * Creates an Chapitre object based on a DB row.
	 *
	 * @param array $row The DB row containing Chapitre data.
	 * @return \MicroCMS\Domain\Chapitre
	 */
	protected function buildDomainObject(array $row) {
		$chapitre = new Chapitre();
		$chapitre->setId($row['id']);
		$chapitre->setTitle($row['title']);
		$chapitre->setSlug($row['slug']);
		$chapitre->setSummary($row['summary']);
		$chapitre->setContent($row['content']);
		$chapitre->setPublished($row['published']);
		return $chapitre;
	}

}