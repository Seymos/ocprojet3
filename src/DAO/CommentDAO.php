<?php

namespace MicroCMS\DAO;

use MicroCMS\Domain\Comment;
use Symfony\Component\Config\Definition\Exception\Exception;

class CommentDAO extends DAO
{
	/**
	 * @var \MicroCMS\DAO\ChapitreDAO
	 */
	private $chapitreDAO;

	public function setChapitreDAO(ChapitreDAO $chapitreDAO) {
		$this->chapitreDAO = $chapitreDAO;
	}

	/**
	 * Returns a list of all comments, sorted by date (most recent first).
	 *
	 * @return array A list of all comments.
	 */
	public function findAll() {
		$sql = "select * from comment order by id desc";
		$result = $this->getDb()->fetchAll($sql);

		// Convert query result to an array of domain objects
		$entities = array();
		foreach ($result as $row) {
			$id = $row['id'];
			$entities[$id] = $this->buildDomainObject($row);
		}
		return $entities;
	}

	/**
	 * Return a list of all comments for an article, sorted by date (most recent last).
	 *
	 * @param integer $articleId The article id.
	 *
	 * @return array A list of all comments for the article.
	 */
	public function findAllByChapitre($chapitreId) {
		// The associated article is retrieved only once
		$chapitre = $this->chapitreDAO->find($chapitreId);

		// art_id is not selected by the SQL query
		// The article won't be retrieved during domain objet construction
		$sql = "select * from comment where chapitre_id=? order by id";
		$result = $this->getDb()->fetchAll($sql, array($chapitre->getId()));

		// Convert query result to an array of domain objects
		$comments = array();
		foreach ($result as $row) {
			$comId = $row['id'];
			$comment = $this->buildDomainObject($row);
			// The associated article is defined for the constructed comment
			$comment->setChapitre($chapitre);
			$comments[$comId] = $comment;
		}
		return $comments;
	}

	/**
	 * Returns a comment matching the supplied id.
	 *
	 * @param integer $id The comment id
	 *
	 * @return \MicroCMS\Domain\Comment|throws an exception if no matching comment is found
	 */
	public function find($id) {
		$sql = "select * from comment where id=?";
		$row = $this->getDb()->fetchAssoc($sql, array($id));

		if ($row)
			return $this->buildDomainObject($row);
		else
			throw new \Exception("No comment matching id " . $id);
	}

	/**
	 * Removes a comment from the database.
	 *
	 * @param @param integer $id The comment id
	 */
	public function delete($id) {
		// Delete the comment
		$this->getDb()->delete('comment', array('id' => $id));
	}

	/**
	 * Removes all comments for an article
	 *
	 * @param $articleId The id of the article
	 */
	public function deleteAllByChapitre($chapitreId) {
		$this->getDb()->delete('comment', array('id' => $chapitreId));
	}

	/**
	 * Saves a comment into the database.
	 *
	 * @param \MicroCMS\Domain\Comment $comment The comment to save
	 */
	public function save(Comment $comment) {
		$commentData = array(
			'pseudo' => $comment->getPseudo(),
			'content' => $comment->getContent(),
			'signals' => $comment->getSignals(),
			'likes' => $comment->getLikes(),
			'chapitre_id' => $comment->getChapitre()->getId(),
		);

		if ($comment->getId()) {
			// The comment has already been saved : update it
			$this->getDb()->update('comment', $commentData, array('id' => $comment->getId()));
		} else {
			$commentData['signals'] = $commentData['likes'] = 0;
			// The comment has never been saved : insert it
			$this->getDb()->insert('comment', $commentData);
			// Get the id of the newly created comment and set it on the entity.
			$id = $this->getDb()->lastInsertId();
			$comment->setId($id);
		}
	}

	/**
	 * Returns a boolean
	 *
	 * @param integer $id The comment id
	 *
	 * @return \MicroCMS\Domain\Comment|throws an exception if no matching comment is found
	 */
	public function signal($id) {
		// récupérer le commentaire en base par son id
		$comment = $this->find($id);
		// récupérer la valeur de son signal
		$signal = $comment->getSignals();
		// augmenter cette valeur
		$signal++;
		// assigner la valeur
		$comment->setSignals($signal);
		// retourner le commentaire
		return $comment;
	}

	/**
	 * Returns a boolean
	 *
	 * @param integer $id The comment id
	 *
	 * @return \MicroCMS\Domain\Comment|throws an exception if no matching comment is found
	 */
	public function unsignal($id) {
		// récupérer le commentaire en base par son id
		$comment = $this->find($id);
		// assigner la valeur
		$comment->setSignals(0);
		$this->save($comment);
		// retourner le commentaire
		return $comment;
	}

	/**
	 * Returns a boolean
	 *
	 * @param integer $id The comment id
	 *
	 * @return \MicroCMS\Domain\Comment|throws an exception if no matching comment is found
	 */
	public function like($id) {
		// récupérer le commentaire en base par son id
		$comment = $this->find($id);
		// récupérer la valeur des likes
		$like = $comment->getLikes();
		// augmenter cette valeur
		$like++;
		// assigner la valeur
		$comment->setLikes($like);
		// retourner le commentaire
		return $comment;
	}

	/**
	 * Creates an Comment object based on a DB row.
	 *
	 * @param array $row The DB row containing Comment data.
	 * @return \MicroCMS\Domain\Comment
	 */
	protected function buildDomainObject(array $row) {
		$comment = new Comment();
		$comment->setId($row['id']);
		$comment->setPseudo($row['pseudo']);
		$comment->setContent($row['content']);
		$comment->setSignals($row['signals']);
		$comment->setLikes($row['likes']);

		if (array_key_exists('chapitre_id', $row)) {
			// Find and set the associated article
			$chapitreId = $row['chapitre_id'];
			$chapitre = $this->chapitreDAO->find($chapitreId);
			$comment->setChapitre($chapitre);
		}

		return $comment;
	}
}