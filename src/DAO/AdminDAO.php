<?php

namespace MicroCMS\DAO;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use MicroCMS\Domain\Admin;

class AdminDAO extends DAO implements UserProviderInterface
{
	/**
	 * Returns a list of all users, sorted by role and name.
	 *
	 * @return array A list of all users.
	 */
	public function findAll() {
		$sql = "select * from admin order by role, username";
		$result = $this->getDb()->fetchAll($sql);

		// Convert query result to an array of domain objects
		$entities = array();
		foreach ($result as $row) {
			$id = $row['id'];
			$entities[$id] = $this->buildDomainObject($row);
		}
		return $entities;
	}

	/**
	 * Returns a user matching the supplied id.
	 *
	 * @param integer $id The user id.
	 *
	 * @return \MicroCMS\Domain\Admin|throws an exception if no matching user is found
	 */
	public function find($id) {
		$sql = "select * from admin where id=?";
		$row = $this->getDb()->fetchAssoc($sql, array($id));

		if ($row)
			return $this->buildDomainObject($row);
		else
			throw new \Exception("No user matching id " . $id);
	}

	/**
	 * {@inheritDoc}
	 */
	public function loadUserByUsername($username)
	{
		$sql = "select * from admin where username=?";
		$row = $this->getDb()->fetchAssoc($sql, array($username));

		if ($row)
			return $this->buildDomainObject($row);
		else
			throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username));
	}

	/**
	 * {@inheritDoc}
	 */
	public function refreshUser(UserInterface $user)
	{
		$class = get_class($user);
		if (!$this->supportsClass($class)) {
			throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $class));
		}
		return $this->loadUserByUsername($user->getUsername());
	}

	/**
	 * {@inheritDoc}
	 */
	public function supportsClass($class)
	{
		return 'MicroCMS\Domain\Admin' === $class;
	}

	/**
	 * Creates a Admin object based on a DB row.
	 *
	 * @param array $row The DB row containing Admin data.
	 *
	 * @return \MicroCMS\Domain\Admin
	 */
	protected function buildDomainObject(array $row) {
		$user = new Admin();
		$user->setId($row['id']);
		$user->setUsername($row['username']);
		$user->setSlug($row['username']);
		$user->setPassword($row['password']);
		$user->setSalt($row['salt']);
		$user->setRole($row['role']);
		return $user;
	}

	/**
	 * Saves a user into the database.
	 *
	 * @param \MicroCMS\Domain\Admin $user The user to save
	 */
	public function save(Admin $user) {
		$userData = array(
			'username' => $user->getUsername(),
			'slug' => $user->getSlug(),
			'salt' => $user->getSalt(),
			'password' => $user->getPassword(),
			'role' => $user->getRole()
		);

		if ($user->getId()) {
			// The user has already been saved : update it
			$this->getDb()->update('admin', $userData, array('id' => $user->getId()));
		} else {
			// The user has never been saved : insert it
			$this->getDb()->insert('admin', $userData);
			// Get the id of the newly created user and set it on the entity.
			$id = $this->getDb()->lastInsertId();
			$user->setId($id);
		}
	}

	/**
	 * Removes a user from the database.
	 *
	 * @param @param integer $id The user id.
	 */
	public function delete($id) {
		// Delete the user
		$this->getDb()->delete('admin', array('id' => $id));
	}
}