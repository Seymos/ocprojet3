<?php

namespace MicroCMS\Domain;

use Doctrine\DBAL\Types\BooleanType;

class Chapitre
{
	/**
	 * Chapitre id.
	 *
	 * @var integer
	 */
	private $id;

	/**
	 * Chapitre title.
	 *
	 * @var string
	 */
	private $title;

	/**
	 * Chapitre slug.
	 *
	 * @var string
	 */
	private $slug;

	/**
	 * Chapitre content.
	 *
	 * @var string
	 */
	private $summary;

	/**
	 * Chapitre content.
	 *
	 * @var string
	 */
	private $content;

	/**
	 * Chapitre published
	 *
	 * @var boolean
	 */
	private $published;

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getTitle() {
		return $this->title;
	}

	public function setTitle($title) {
		$this->title = $title;
		return $this;
	}

	public function getSlug() {
		return $this->slug;
	}

	public function setSlug($slug) {
		$this->slug = $slug;
		return $this;
	}

	public function getSummary() {
		return $this->summary;
	}

	public function setSummary($summary) {
		$this->summary = $summary;
		return $this;
	}

	public function getContent() {
		return $this->content;
	}

	public function setContent($content) {
		$this->content = $content;
		return $this;
	}

	public function getPublished() {
		return $this->published;
	}

	public function setPublished($published) {
		$this->published = $published;
		return $this;
	}
}