<?php
namespace MicroCMS\Domain;

use Symfony\Component\Validator\Constraints as Assert;

class Comment
{
	/**
	 * Comment id.
	 *
	 * @var integer
	 */
	private $id;

	/**
	 * Comment pseudo.
	 *
	 * @var string
	 */
	private $pseudo;

	/**
	 * Comment content.
	 *
	 * @var integer
	 */
	private $content;

	/**
	 * Comment signals
	 *
	 * @var integer
	 */
	private $signals;

	/**
	 * Comment likes
	 *
	 * @var integer
	 */
	private $likes;

	/**
	 * Associated chapitre.
	 *
	 * @var \MicroCMS\Domain\Chapitre
	 */
	private $chapitre;

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function setPseudo($pseudo) {
		$this->pseudo = $pseudo;
		return $this;
	}

	public function getPseudo() {
		return $this->pseudo;
	}

	public function setSignals($signals) {
		$this->signals = $signals;
		return $this;
	}

	public function getSignals() {
		return $this->signals;
	}

	public function setLikes($likes) {
		$this->likes = $likes;
		return $this;
	}

	public function getLikes() {
		return $this->likes;
	}

	public function getContent() {
		return $this->content;
	}

	public function setContent($content) {
		$this->content = $content;
		return $this;
	}

	public function getChapitre() {
		return $this->chapitre;
	}

	public function setChapitre(Chapitre $chapitre) {
		$this->chapitre = $chapitre;
		return $this;
	}
}