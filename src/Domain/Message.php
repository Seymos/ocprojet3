<?php

namespace MicroCMS\Domain;

use Symfony\Component\Validator\Constraints\DateTime;

class Message {
	/**
	 * Message id.
	 *
	 * @var integer
	 */
	private $id;

	/**
	 * Message pseudo.
	 *
	 * @var string
	 */
	private $name;

	/**
	 * Message email.
	 *
	 * @var string
	 */
	private $email;

	/**
	 * Message message.
	 *
	 * @var string
	 */
	private $message;

	/**
	 * Message date
	 *
	 * @var string
	 */
	private $dateCreation;

	/**
	 * @param int $id
	 */
	public function setId( $id ) {
		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param string $name
	 */
	public function setName( $name ) {
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail( $email ) {
		$this->email = $email;
	}

	/**
	 * @return string
	 */
	public function getMessage() {
		return $this->message;
	}

	/**
	 * @param string $message
	 */
	public function setMessage( $message ) {
		$this->message = $message;
	}

	/**
	 * @return string
	 */
	public function getDateCreation() {
		return $this->dateCreation;
	}

	/**
	 * @param string $dateCreation
	 */
	public function setDateCreation($dateCreation) {
		$this->dateCreation = $dateCreation;
	}
}